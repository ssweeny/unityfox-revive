/* jshint moz: true */
/* -*- Mode: javascript; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* ***** BEGIN LICENSE BLOCK *****
 *	 Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is unityfox
 *
 * The Initial Developer of the Original Code is
 * Chris Coulson
 * Portions created by the Initial Developer are Copyright (C) 2011
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *    Lockal <lockalsash@gmail.com>
 *    Chris Coulson <chrisccoulson@ubuntu.com>
 *    Zhanibek Adilbekov <zhanibek@archlinux.info>
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK ***** */

var { Cu, Ci, Cc } = require("chrome");

var gobject = null;
var unity = null;

const nsIXULAppInfo         = Ci.nsIXULAppInfo;

Cu.import("resource://gre/modules/Services.jsm");

var gProgressIndicator = null;

function ProgressIndicator() {
  console.log("Creating new progress indicator instance");
  if (gProgressIndicator) {
    throw "You can't create more than one progress indicator instance";
  }
}

ProgressIndicator.prototype = {
  _entry: null,
  _loaded: false,

  get entry() {
    if (this._entry)
      return this._entry;

    let appName = Cc["@mozilla.org/xre/app-info;1"]
                  .getService(nsIXULAppInfo).name.toLowerCase();
    this._entry = unity.unity_launcher_entry_get_for_desktop_id(appName + ".desktop");

    return this._entry;
  },

  set entry(aEntry) {
    if (this._entry) {
      gobject.g_object_unref(this._entry);
    }

    this._entry = aEntry;

    if (this._entry) {
      gobject.g_object_ref(this._entry);
    }
  },

  setCount: function PI_setCount(count) {
    unity.unity_launcher_entry_set_count(this.entry, count);
  },

  setProgress: function PI_setProgress(progress) {
    unity.unity_launcher_entry_set_progress(this.entry, progress);
  },

  hide: function PI_hide() {
    unity.unity_launcher_entry_set_progress_visible(this.entry, false);
    unity.unity_launcher_entry_set_count_visible(this.entry, false);
  },

  show: function PI_hide() {
    unity.unity_launcher_entry_set_progress_visible(this.entry, true);
    unity.unity_launcher_entry_set_count_visible(this.entry, true);
  },

  init: function PI_Init() {
    if (!this._loaded) {
      console.log("Loading required JS modules");

      if (gobject === null)
        gobject = require('./gobject.js');
      if (unity === null)
        unity = require('./unity.js');

      if (!unity) {
        console.warn("The required libraries aren't available to run this extension");
        this.destroy();
        gProgressIndicator = null;
        return false;
      }
      this._loaded = true;
      return true;
    }
  },

  destroy: function PI_destroy() {
    if (this._loaded) {
      this.hide();
      this.entry = null;
    }
  }
};

module.exports = ProgressIndicator;
