Unityfox Revived
================

This Firefox extension adds a download progress bar and active downloads count to the Firefox icon on the launcher in Unity and KDE Plasma 5.6+. Inspired by original [Unityfox](https://addons.mozilla.org/en-US/firefox/addon/unityfox/) add-on by [Chris Coulson](https://addons.mozilla.org/en-US/firefox/user/chrisccoulson/) and [Lockal](https://addons.mozilla.org/en-US/firefox/user/lockal/)
